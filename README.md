Coroutines in Practice
====================

This is the source code for the talk "Coroutines in Pratice" in the
Italian C++ Conference 2019 (https://www.italiancpp.org/event/itcppcon19/)

Slides from the presentation can be found in the slides folder.

Contents
---------------------

The code implements a very simple coroutine-based, single-thread framework and
uses it to implement a named pipe communication between two applications.

Requirements
---------------------

The code compiles with Visual Studio 2019 and has no external dependencies.
Since it uses Windows named pipes, it will not compile under other OSs, although
the coroutine framework can easily be adapted for other uses.

Main differences between the code and the slides
---------------------
For time constraints, the code presented on the slides has been simplified
considerably. The code in this repository is more complete. In particular:

* The Client and Server examples do a bit more than exchanging a message
* The classes RawConnection, ServerLoop and ClientLoop implement a more
complex client/server logic upon the Pipe infrastructure
* The Coroutine<T> template has a detach() method that allows you to 
let a coroutine run even if the coroutine object is destroyed (much
similar to std::thread::detach() for threads). A detached coroutine object
cannot be co_await-ed
* Error handling is fully implemented using exceptions

Acknoledgements
---------------------
The file Common/span.hpp is Copyright Tristan Brindle 2018 and is
distributed under the Boost Software License, Version 1.0. The original source can
be found at [on Tristan's github page](https://github.com/tcbrindle/span)