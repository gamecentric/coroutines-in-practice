#pragma once

#include <system_error>

namespace gamecentric
{
	[[noreturn]] inline void ThrowOsError(DWORD error, const char* description)
	{
		if (error == ERROR_BROKEN_PIPE)
		{
			// Workaround: ERROR_BROKEN_PIPE should be equivalent to std::errc::broken_pipe but isn't
			throw std::system_error(std::make_error_code(std::errc::broken_pipe), description);
		}
		else
		{
			throw std::system_error(std::error_code(error, std::system_category()), description);
		}
	}

	[[noreturn]] inline void ThrowOsError(const char* description)
	{
		ThrowOsError(GetLastError(), description);
	}
}