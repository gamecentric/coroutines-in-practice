#pragma once

#include "Pipe.h"
#include "Coroutine.h"

namespace gamecentric
{
	template <class ConnectionType>
	class ClientLoop
	{
	public:
		template <class... Args>
		ClientLoop(const wchar_t* name, Args&& ... constructionArgs)
			: m_connectionLoop{ connectionLoop(name, std::forward<Args>(constructionArgs)...) }
		{}

	private:
		Coroutine<> m_connectionLoop;

		template <class... Args>
		Coroutine<> connectionLoop(const wchar_t* name, Args... constructionArgs) // no forwarding references
		{
			Pipe pipe = co_await Pipe::openPipe(name);
			ConnectionType connection{ std::move(pipe), pipe.bufferSize(), std::move(constructionArgs)... }; // move, not forward
			co_await connection.run();
		}
	};
}