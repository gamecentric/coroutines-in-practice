#pragma once

#include "CoroLoop.h"
#include <optional>
#include <cassert>

namespace gamecentric
{
	template <class T> class Coroutine;

	namespace detail
	{
		template <class T>
		struct BasicPromise
		{
			std::optional<T> m_value;

			void return_value(T v)
			{
				m_value = std::move(v);
			}

			T value()
			{
				assert(m_value.has_value());
				return std::move(*m_value);
			}
		};

		template <class T>
		struct BasicPromise<T&>
		{
			T* m_value = nullptr;

			void return_value(T& v) noexcept
			{
				m_value = &v;
			}

			T& value() noexcept
			{
				assert(m_value);
				return *m_value;
			}
		};

		template <>
		struct BasicPromise<void>
		{
			void return_void() noexcept
			{
			}

			void value() noexcept
			{}
		};

		template <class T>
		struct Promise : BasicPromise<T>
		{
			std::exception_ptr m_exception;
			coroutine_handle<> m_awaitingCoro;
			bool m_detached = false;

			Promise() = default;
			Promise(const Promise&) = delete;
			Promise& operator=(const Promise&) = delete;

			Coroutine<T> get_return_object() noexcept
			{
				return { coroutine_handle<Promise>::from_promise(*this) };
			}

			auto initial_suspend() noexcept
			{
				return suspend_never{};
			}

			auto& final_suspend() noexcept
			{
				// the promise itself acts as the final suspend awaiter
				return *this;
			}

			void unhandled_exception() noexcept
			{
				m_exception = std::current_exception();
			}

			void detach()
			{
				assert(m_detached == false && m_awaitingCoro == nullptr);
				m_detached = true;
			}

			void awaitsCoro(coroutine_handle<> coro)
			{
				assert(m_detached == false && m_awaitingCoro == nullptr);
				m_awaitingCoro = coro;
			}

			T value()
			{
				if (m_exception)
				{
					std::rethrow_exception(m_exception);
				}
				return BasicPromise<T>::value();
			}

			bool await_ready() const noexcept
			{
				return m_detached;
			}

			void await_suspend(coroutine_handle<>) const
			{
				assert(m_detached == false); // this is guaranteed because await_ready() evaluated to false
				if (m_awaitingCoro)
				{
					CoroLoop::getLoop().schedule(m_awaitingCoro);
				}
			}

			// With symmetric coroutines, await_suspend would become:
			//coroutine_handle<> await_suspend(coroutine_handle<>) const
			//{
			//	assert(m_detached == false);
			//	return m_awaitingCoro ? m_awaitingCoro ? std::noop_coroutine();
			//}

			void await_resume() const noexcept
			{
			}
		};
	}

	template <class T = void>
	class [[nodiscard]] Coroutine
	{
	public:
		using promise_type = detail::Promise<T>;

		Coroutine() = default;

		~Coroutine() noexcept
		{
			if (m_coro)
			{
				m_coro.destroy();
			}
		}

		Coroutine(const Coroutine&) = delete;
		Coroutine& operator=(const Coroutine&) = delete;

		Coroutine(Coroutine&& rhs)  noexcept : m_coro(rhs.m_coro)
		{
			rhs.m_coro = nullptr;
		}

		Coroutine& operator=(Coroutine&& rhs) noexcept
		{
			if (m_coro != rhs.m_coro)
			{
				if (m_coro)
				{
					m_coro.destroy();
				}
				m_coro = rhs.m_coro;
				rhs.m_coro = nullptr;
			}
			return *this;
		}

		void detach() noexcept
		{
			assert(m_coro);
			if (m_coro.done())
			{
				m_coro.destroy();
			}
			else
			{
				m_coro.promise().detach();
			}
			m_coro = nullptr;
		}

		bool await_ready() const noexcept
		{
			assert(m_coro);
			return m_coro.done();
		}

		void await_suspend(coroutine_handle<> h) const noexcept
		{
			assert(m_coro);
			m_coro.promise().awaitsCoro(h);
		}

		T await_resume() const
		{
			assert(m_coro);
			return m_coro.promise().value();
		}

	private:
		friend Coroutine promise_type::get_return_object() noexcept;

		Coroutine(coroutine_handle<promise_type> h) noexcept : m_coro(h)
		{}

		coroutine_handle<promise_type> m_coro;
	};
}