#pragma once

#include "Handle.h"

namespace gamecentric
{
	class Event : public Handle
	{
	public:
		Event()
			: Handle(CreateEvent(nullptr, false, false, nullptr))
		{}

		OVERLAPPED makeOverlapped() const
		{
			OVERLAPPED ol { };
			ol.hEvent = handle();
			return ol;
		}
	};
}