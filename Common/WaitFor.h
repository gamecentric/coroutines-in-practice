#pragma once

#include "Handle.h"
#include <chrono>

namespace gamecentric
{
	class WaitFor
	{
	public:
		// hundred nanoseconds
		using hns = std::chrono::duration<long long, std::ratio<100, 1'000'000'000>>;

		template <class Rep, class Period>
		WaitFor(std::chrono::duration<Rep, Period> interval)
			: m_interval(std::chrono::duration_cast<hns>(interval))
		{}

		bool await_ready() const noexcept
		{
			m_timer.await_ready();
			return m_interval <= hns::zero();
		}

		void await_suspend(coroutine_handle<> h)
		{
			m_timer.set(CreateWaitableTimer(nullptr, false, nullptr));
			LARGE_INTEGER dueTime;
			dueTime.QuadPart = -m_interval.count();
			SetWaitableTimer(m_timer.handle(), &dueTime, 0, nullptr, nullptr, false);
			m_timer.await_suspend(h);
		}

		void await_resume()
		{
			m_timer.await_resume();
		}

	private:
		hns m_interval;
		Handle m_timer;
	};

	namespace operators
	{
		template <class Rep, class Period>
		WaitFor operator co_await (std::chrono::duration<Rep, Period> interval)
		{
			return { interval };
		}
	}
}