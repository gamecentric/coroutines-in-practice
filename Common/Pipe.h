#pragma once

#include "Handle.h"
#include "Coroutine.h"

#include <cstddef>
#include <vector>
#include <array>
#include <string_view>
#include "span.hpp"

namespace gamecentric
{
	class Pipe
	{
	public:
		Pipe(Pipe&& c) = default;
		Pipe& operator=(Pipe&& c) = default;

		static Coroutine<Pipe> createPipe(const wchar_t* name, unsigned bufferSize);
		static Coroutine<Pipe> openPipe(const wchar_t* name);

		Coroutine<size_t> read(tcb::span<std::byte> buffer);
		Coroutine<> write(tcb::span<const std::byte> data);
		void shutdown();

		unsigned bufferSize() const
		{
			return m_bufferSize;
		}

		HANDLE handle() const
		{
			return m_handle.handle();
		}

	private:
		Handle m_handle;
		unsigned m_bufferSize;

		explicit Pipe(HANDLE handle, unsigned bufferSize)
			: m_handle{ handle }, m_bufferSize{ bufferSize }
		{}
	};
}