#include "stdafx.h"

#include "Event.h"
#include "Pipe.h"
#include "WaitFor.h"

namespace gamecentric
{
	Coroutine<Pipe> Pipe::createPipe(const wchar_t* name, unsigned bufferSize)
	{
		HANDLE handle = CreateNamedPipe(name,
			PIPE_ACCESS_DUPLEX | FILE_FLAG_OVERLAPPED,
			PIPE_TYPE_MESSAGE | PIPE_READMODE_MESSAGE,
			PIPE_UNLIMITED_INSTANCES,
			bufferSize, bufferSize, 0, nullptr);

		if (handle == INVALID_HANDLE_VALUE)
		{
			ThrowOsError("CreateNamedPipe failed");
		}

		Pipe pipe { handle, bufferSize };

		Event ev;
		OVERLAPPED ol = ev.makeOverlapped();
		DWORD result = ConnectNamedPipe(pipe.handle(), &ol);
		assert(result == 0); // ConnectNamedPipe always return 0, when executed asynchronously
		DWORD error = GetLastError();
		if (error == ERROR_PIPE_CONNECTED)
		{
			// nothing to do
		}
		else if (error != ERROR_IO_PENDING)
		{
			ThrowOsError(error, "ConnectNamedPipe failed");
		}
		else
		{
			co_await ev;
		}
		co_return std::move(pipe);
	}

	Coroutine<Pipe> Pipe::openPipe(const wchar_t* name)
	{
		for (;;)
		{
			HANDLE handle = CreateFile(name, GENERIC_READ | GENERIC_WRITE, 0, nullptr, OPEN_EXISTING, FILE_FLAG_OVERLAPPED, nullptr);
			if (handle == INVALID_HANDLE_VALUE)
			{
				DWORD error = GetLastError();
				if (error != ERROR_FILE_NOT_FOUND)
				{
					ThrowOsError("CreateFile failed");
				}

				using namespace std::chrono_literals;
				using namespace gamecentric::operators;
				co_await 1s;
			}
			else
			{
				DWORD mode = PIPE_READMODE_MESSAGE;
				if (SetNamedPipeHandleState(handle, &mode, nullptr, nullptr) == 0)
				{
					CloseHandle(handle);
					ThrowOsError("SetNamedPipeHandleState failed");
				}

				DWORD bufSize;
				if (GetNamedPipeInfo(handle, nullptr, nullptr, &bufSize, nullptr) == 0)
				{
					CloseHandle(handle);
					ThrowOsError("GetNamedPipeInfo failed");
				}
				co_return Pipe{ handle, bufSize };
			}
		}
	}

	Coroutine<size_t> Pipe::read(tcb::span<std::byte> buffer)
	{
		Event ev;
		OVERLAPPED ol = ev.makeOverlapped();
		if (ReadFile(handle(), buffer.data(), static_cast<DWORD>(buffer.size()), nullptr, &ol) == 0)
		{
			DWORD error = GetLastError();
			if (error != ERROR_IO_PENDING)
			{
				ThrowOsError(error, "ReadFile");
			}
			co_await ev;
		}

		DWORD bytesRead;
		if (GetOverlappedResult(handle(), &ol, &bytesRead, false))
		{
			co_return bytesRead;
		}
		else
		{
			ThrowOsError("GetOverlappedResult");
		}
	}

	Coroutine<> Pipe::write(tcb::span<const std::byte> data)
	{
		Event ev;
		OVERLAPPED ol = ev.makeOverlapped();
		if (WriteFile(handle(), data.data(), static_cast<DWORD>(data.size()), nullptr, &ol) == 0)
		{
			DWORD error = GetLastError();
			if (error != ERROR_IO_PENDING)
			{
				ThrowOsError(error, "WriteFile");
			}
			co_await ev;
		}

		DWORD bytesWritten;
		if (GetOverlappedResult(handle(), &ol, &bytesWritten, false) == 0)
		{
			ThrowOsError("GetOverlappedResult");
		}
	}

	void Pipe::shutdown()
	{
		CancelIo(handle());
	}
}