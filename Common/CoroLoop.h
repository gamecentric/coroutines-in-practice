#pragma once

#if __has_include(<coroutine>)
#include <coroutine>
#elif __has_include(<experimental/coroutine>)
#include <experimental/coroutine>
#else
#error No coroutine support
#endif

#include <vector>
#include <functional>
#include <cassert>

namespace gamecentric
{
#if __has_include(<coroutine>)
	using std::suspend_never;
	using std::coroutine_handle;
#else
	using std::experimental::suspend_never;
	using std::experimental::coroutine_handle;
#endif

	/// The Coroutine Loop.
	/// This class implements a single-thread, coroutine-based cooperative event loop.
	/// Coroutines can be scheduled in the loop using the schedule() function.
	/// The loop will continue running until there is at least one scheduled coroutine.
	/// All calls to an CoroLoop object must be performed from the same thread.
	/// This means that once run() is called, all subsequent calls will be performed
	/// by a scheduled coroutine.
	class CoroLoop
	{
	public:
		CoroLoop();
		~CoroLoop();

		CoroLoop(const CoroLoop&) = delete;
		CoroLoop& operator=(const CoroLoop&) = delete;

		/// Runs the event loop.
		void run();

		/// Schedule a "ready" coroutine. The coroutine will be executed as soon as the
		/// execution flow returns to the event loop. Multiple coroutines may be scheduled
		/// at a given time and they will be executed on a first-come, first-served base.
		void schedule(coroutine_handle<> coro);

		/// Schedule a function object. This function effectively wraps \arg f into a coroutine
		/// and then schedules such coroutine as "ready".
		void schedule(std::function<void()> f);

		/// Schedules a coroutine that is "blocked" on a system handle. The coroutine will be executed
		/// as soon as the handle enters the "signaled" state. Refer to WaitForMultipleObjectsEx
		/// in the Windows documentation.
		void schedule(HANDLE handle, coroutine_handle<> coro);

		/// Unscheduled a coroutine that was scheduled with schedule(HANDLE, coroutine_handle<>).
		/// This may be necessary if the coroutine is abandoned before the handle enters the "signaled" state.
		void unschedule(HANDLE handle);

		/// Gets a reference to a global CoroLoop instance.
		static CoroLoop& getLoop()
		{
			assert(s_loop);
			return *s_loop;
		}

	private:
		int pendingCoroCount = 0;
		std::vector<HANDLE> m_handles;
		std::vector<coroutine_handle<>> m_blocked;
		std::vector<coroutine_handle<>> m_ready;

		static CoroLoop* s_loop;
	};

	/// Awaiter helper to allow a coroutine to block and be 
	struct NextTickAwaiter
	{
		bool await_ready() const
		{
			return false;
		}

		void await_suspend(coroutine_handle<> h) const
		{
			CoroLoop::getLoop().schedule(h);
		}

		void await_resume() const
		{
		}
	};

	constexpr inline NextTickAwaiter nextTick{};
}