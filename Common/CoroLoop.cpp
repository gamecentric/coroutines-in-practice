#include "stdafx.h"
#include "CoroLoop.h"
#include "OsError.h"
#include "Coroutine.h"
#include <cassert>

namespace gamecentric
{
	CoroLoop* CoroLoop::s_loop;

	CoroLoop::CoroLoop()
	{
		assert(s_loop == nullptr);
		s_loop = this;
	}

	CoroLoop::~CoroLoop()
	{
		assert(s_loop == this);
		for (HANDLE h : m_handles)
		{
			CloseHandle(h);
		}
		s_loop = nullptr;
	}

	void CoroLoop::run()
	{
		while (!m_handles.empty() || !m_ready.empty())
		{
			DWORD timeout = m_ready.empty() ? INFINITE : 0;

			if (m_handles.empty())
			{
				// can't use WaitForMultipleObjectsEx with 0 handles
				DWORD result = SleepEx(timeout, true);
				if (result != 0 && result != WAIT_IO_COMPLETION)
				{
					ThrowOsError("SleepEx failed");
				}
			}
			else
			{
				DWORD result = WaitForMultipleObjectsEx((DWORD)m_handles.size(), m_handles.data(), false, timeout, true);
				if (result >= WAIT_OBJECT_0 && result < WAIT_OBJECT_0 + m_handles.size())
				{
					unsigned index = result - WAIT_OBJECT_0;
					auto coro = m_blocked[index];
					m_handles.erase(m_handles.begin() + index);
					m_blocked.erase(m_blocked.begin() + index);
					coro.resume();
				}
				else if (result != WAIT_IO_COMPLETION && result != WAIT_TIMEOUT)
				{
					ThrowOsError("WaitForMultipleObjectsEx failed");
				}
			}

			std::vector<coroutine_handle<>> ready;
			ready.swap(m_ready);
			for (auto coro : ready)
			{
				coro.resume();
			}
		}
	}

	void CoroLoop::schedule(coroutine_handle<> coro)
	{
		assert(coro);
		m_ready.push_back(coro);
	}

	void CoroLoop::schedule(HANDLE handle, coroutine_handle<> coro)
	{
		assert(coro);
		assert(handle);
		m_handles.push_back(handle);
		m_blocked.push_back(coro);
	}

	void CoroLoop::unschedule(HANDLE handle)
	{
		assert(handle);
		auto it = std::find(m_handles.begin(), m_handles.end(), handle);
		assert(it != m_handles.end());
		auto bit = std::next(m_blocked.begin(), std::distance(m_handles.begin(), it));
		m_handles.erase(it);
		m_blocked.erase(bit);
	}

	void CoroLoop::schedule(std::function<void()> f)
	{
		[](std::function<void()> f) -> Coroutine<>
		{
			co_await nextTick;
			f();
		}(std::move(f)).detach();
	}
}