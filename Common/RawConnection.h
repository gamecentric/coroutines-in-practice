#pragma once

#include "Pipe.h"
#include "CoroLoop.h"
#include "Coroutine.h"
#include <functional>
#include "span.hpp"

namespace gamecentric
{
	template <class ConnectionType>
	class RawConnection
	{
	public:
		RawConnection(Pipe&& pipe, unsigned bufferSize)
			: m_state{ State::Constructed }
			, m_pipe{ std::move(pipe) }
			, m_bufferSize{ bufferSize }
		{}

		~RawConnection() = default;
		RawConnection(const RawConnection&) = delete;
		RawConnection& operator=(const RawConnection&) = delete;
		RawConnection(RawConnection&&) = default;
		RawConnection& operator=(RawConnection&&) = default;

		Coroutine<> write(std::vector<std::byte> msg)
		{
			assert(m_state == State::Connected);
			co_await m_pipe.write(msg);
		}

		void post(std::vector<std::byte> msg)
		{
			[](RawConnection * conn, std::vector<std::byte> msg) ->Coroutine<> {
				co_await conn->write(std::move(msg));
			} (this, std::move(msg)).detach();
		}

		void onConnect() {}
		void onIncomingMessage(tcb::span<std::byte> msg) = delete; // to be defined in subclasses
		void onDisconnect() {}

		Coroutine<> run()
		{
			assert(m_state == State::Constructed);

			m_state = State::Connected;
			connection()->onConnect();

			std::vector<std::byte> buf(m_bufferSize);
			try
			{
				for (;;)
				{
					auto size = co_await m_pipe.read(buf);
					connection()->onIncomingMessage({ buf.data(), buf.data() + size });
				}
			}
			catch (const std::system_error& e)
			{
				auto errc = e.code().default_error_condition();
				if (errc != std::errc::broken_pipe && errc != std::errc::operation_canceled)
				{
					throw;
				}
			}

			m_state = State::Disconnected;
			connection()->onDisconnect();
		}

		void shutdown()
		{
			assert(m_state == State::Connected);
			m_pipe.shutdown();
		}

		ConnectionType* connection()
		{
			return static_cast<ConnectionType*>(this);
		}

		const ConnectionType* connection() const
		{
			return static_cast<const ConnectionType*>(this);
		}

	private:
		enum class State { Constructed, Connected, Disconnected };
		State m_state;
		Pipe m_pipe;
		unsigned m_bufferSize;
	};
}