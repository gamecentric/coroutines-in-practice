#pragma once

#include "CoroLoop.h"
#include "OsError.h"
#include <cassert>

namespace gamecentric
{
	class Handle
	{
		HANDLE m_handle;
		bool m_scheduled;

	public:
		Handle() noexcept
			: m_handle{ nullptr }
			, m_scheduled{ false }
		{}

		explicit Handle(HANDLE handle) noexcept
			: m_handle{ handle }
			, m_scheduled{ false }
		{}

		~Handle() noexcept
		{
			if (m_handle)
			{
				if (m_scheduled)
				{
					CoroLoop::getLoop().unschedule(m_handle);
				}
				CloseHandle(m_handle);
			}
		}

		Handle(const Handle&) = delete;
		Handle& operator=(const Handle&) = delete;

		Handle(Handle&& c) noexcept
			: m_handle{ c.m_handle }
			, m_scheduled{ c.m_scheduled }
		{
			c.m_handle = nullptr;
		}

		Handle& operator=(Handle&& c) noexcept
		{
			if (m_handle != c.m_handle)
			{
				if (m_handle)
				{
					CloseHandle(m_handle);
				}

				m_handle = c.m_handle;
				m_scheduled = c.m_scheduled;
				c.m_handle = nullptr;
			}
			return *this;
		}

		explicit operator bool() const noexcept
		{
			return m_handle != nullptr;
		}

		bool operator ! () const noexcept
		{
			return m_handle == nullptr;
		}

		void set(HANDLE h)
		{
			assert(m_handle == nullptr);
			assert(m_scheduled == false);
			m_handle = h;
		}

		HANDLE handle() const noexcept
		{
			return m_handle;
		}

		bool await_ready() const noexcept
		{
			assert(m_scheduled == false);
			return false;
		}

		void await_suspend(coroutine_handle<> h)
		{
			m_scheduled = true;
			CoroLoop::getLoop().schedule(m_handle, h);
		}

		void await_resume() noexcept
		{
			m_scheduled = false;
		}
	};
}