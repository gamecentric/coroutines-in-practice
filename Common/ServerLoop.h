#pragma once

#include "Pipe.h"
#include "Coroutine.h"
#include <list>

namespace gamecentric
{
	template <class ConnectionType>
	class ServerLoop
	{
	public:
		template <class... Args>
		ServerLoop(const wchar_t* name, unsigned bufferSize, Args&& ... constructionArgs)
			: m_loop{ run(name, bufferSize, std::forward<Args>(constructionArgs)...) }
		{}

	private:
		std::list<ConnectionType> activeConnections;
		Coroutine<> m_loop;

		template <class... Args>
		Coroutine<> run(const wchar_t* name, unsigned bufferSize, Args... constructionArgs) // no forwarding references
		{
			for (;;)
			{
				Pipe pipe = co_await Pipe::createPipe(name, bufferSize);

				activeConnections.emplace_front(std::move(pipe), bufferSize, constructionArgs...); // no forward, nor move

				[](auto& container, auto it) -> Coroutine<>
				{
					co_await it->run();
					container.erase(it);
				}(activeConnections, activeConnections.begin()).detach();
			}
		}
	};
}