// TestClient.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include "pch.h"
#include "RawConnection.h"
#include "ClientLoop.h"
#include "WaitFor.h"
#include <iostream>
#include <string>
#include <sstream>

using namespace gamecentric;

std::vector<std::byte> stringToBytes(const std::string& s)
{
	const std::byte* bytes = reinterpret_cast<const std::byte*>(s.data());
	return { bytes, bytes + s.size() };
}

std::string bytesToString(tcb::span<std::byte> b)
{
	const char* chars = reinterpret_cast<const char*>(b.data());
	return { chars, chars + b.size() };
}

struct ClientConnection : RawConnection<ClientConnection>
{
	using RawConnection<ClientConnection>::RawConnection;

	void onConnect()
	{
		using namespace std::chrono_literals;
		using namespace gamecentric::operators;

		std::cout << "Connected to server" << "\n";

		[](ClientConnection* conn) -> Coroutine<> {

			std::istringstream input{ "This was a triumph! I'm making a note here: Huge success!" };

			for (std::string s; input >> s;)
			{
				co_await conn->write(stringToBytes(s));
				co_await 1s;
			}
			conn->shutdown();
		}(this).detach();
	}

	void onIncomingMessage(tcb::span<std::byte> msg)
	{
		std::cout << "Received message: " << bytesToString(msg) << "\n";
	}

	void onDisconnect()
	{
		std::cout << "Disconnected from server" << "\n";
	}
};

int main()
{
	std::cout << "Client started.\n";

	CoroLoop loop;
	ClientLoop<ClientConnection> client { LR"(\\.\pipe\TestPipe)" };

	loop.run();
	std::cout << "Client exiting.\n";
}
