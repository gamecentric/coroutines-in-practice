// TestServer.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include "pch.h"
#include "RawConnection.h"
#include "ServerLoop.h"
#include <iostream>
#include <string>

using namespace gamecentric;

std::vector<std::byte> stringToBytes(const std::string& s)
{
	const std::byte* bytes = reinterpret_cast<const std::byte*>(s.data());
	return { bytes, bytes + s.size() };
}

std::string bytesToString(tcb::span<std::byte> b)
{
	const char* chars = reinterpret_cast<const char*>(b.data());
	return { chars, chars + b.size() };
}

struct ServerConnection : RawConnection<ServerConnection>
{
	using RawConnection<ServerConnection>::RawConnection;

	void onConnect()
	{
		std::cout << "Client connected" << "\n";
		post(stringToBytes("Hello, client"));
	}

	void onIncomingMessage(tcb::span<std::byte> msg)
	{
		std::string s = bytesToString(msg);
		std::cout << "Received message: " << s << "\n";
		std::reverse(s.begin(), s.end());
		post(stringToBytes(s));
	}

	void onDisconnect()
	{
		std::cout << "Client disconnected" << "\n";
	}
};

int main()
{
	std::cout << "Server started.\n";
	CoroLoop loop;

	ServerLoop<ServerConnection> server(LR"(\\.\pipe\TestPipe)", 4096);

	loop.run();
    std::cout << "Server exiting.\n";
}
